import * as React from "react";
import { Html, Head, Main, NextScript } from "next/document";
import theme from "./theme";

export default function Document() {
  return (
    <Html>
      <Head>
        <meta content={theme.palette.primary.main} name="theme-color" />
        <link rel={"manifest"} href={"/manifest.webmanifest"} />
        <link href="../../public/favicon.ico" rel="shortcut icon" />

        {/* Apple Login */}
        {/*<script*/}
        {/*  type="text/javascript"*/}
        {/*  src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js"*/}
        {/*  async*/}
        {/*/>*/}

        {/*CSS for Quill*/}
        {/*<link*/}
        {/*  href="https://cdn.quilljs.com/1.3.6/quill.snow.css"*/}
        {/*  rel="stylesheet"*/}
        {/*/>*/}

        {/* Google Fonts */}
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Epilogue:wght@300;500;600;700;800;900&display=swap"
          rel="stylesheet"
        />
        <link href="https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Space+Grotesk:wght@300;400;500;600;700&display=swap" rel="stylesheet" />

        <title>{process.env.name}</title>
        
        {/*{this.props.emotionStyleTags}*/}
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
