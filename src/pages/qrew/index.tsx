import { Box, Grid } from '@mui/material'
import React from 'react'
import CustomAppBar from '../components/CustomAppBar'
import TopSection from '../components/TopSection'
import PlayZone from '../components/PlayZone'
import DineZone from '../components/DineZone'
import CustomSlider from '../components/CustomSlider'
import { Carousel } from '../components/Carousel'
import { PartZoneSection } from '../components/PartyZone'
import { ExperienceSection } from '../components/ExperienceSection'
import { GallerySection } from '../components/GallerySection'
import Footer from '../components/Footer'

function Qrew() {
  return (
    <Grid width={'100%'} display={"flex"} flexDirection={"column"} bgcolor={'#070812'}>
        <CustomAppBar />
        <TopSection />
        <Carousel />
        <PlayZone />
        <DineZone />
        <PartZoneSection />
        <ExperienceSection />
        <GallerySection />
        <Footer />
    </Grid>
  )
}

export default Qrew