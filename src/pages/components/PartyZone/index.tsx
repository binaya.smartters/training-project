/* eslint-disable @next/next/no-img-element */
import React, {useEffect, useState} from "react";
import Box from "@mui/material/Box";
import {Button, Typography} from "@mui/material";
// @ts-ignore
import Slider, { Settings } from 'react-slick';
import SliderSection from "../SliderSection/SliderSection";

interface PartZoneSectionProps {}

export const PartZoneSection: React.FC<PartZoneSectionProps> = () => {

    const [imageIndex, setImageIndex] = useState(1);

    const data = [
        {
          attachment:'/images/Rectangle 1092.svg',
          title:'PC/ CONSOLE/ ESPORTS',
          subtitle:'VR RACING/DRIVING SIMULATOR'
        },{
          attachment:'/images/Rectangle 1092.svg',
          title:'PC/ CONSOLE/ ESPORTS',
          subtitle:'VR RACING/DRIVING SIMULATOR'
        },{
          attachment:'/images/Rectangle 1092.svg',
          title:'PC/ CONSOLE/ ESPORTS',
          subtitle:'VR RACING/DRIVING SIMULATOR'
        },
      ]
    
    const settings = {
        infinite: false,
        lazyLoad: true,
        speed: 300,
        slidesToShow: 3,
        initialSlide: 1,
        centerMode: true,
        centerPadding: 0,
        swipeToSlide: true,
        // nextArrow: <NextArrow onClick={undefined}/>,
        // prevArrow: <PrevArrow onClick={undefined} />,
        beforeChange: (current:any, next:any) => setImageIndex(next),
        // afterChange: function(index) {
        //     console.log(
        //         `Slider Changed to: ${index + 1}, background: #222; color: #bada55`
        //     );
        // },
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: false,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };


    return (
      
        <Box
        display={"flex"}
        flexDirection={"column"}
        sx={{
            background: `url('/image/partyzone.svg')`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            width: '100%',
        }}
    >
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'} justifyContent={'center'} mb={3}>
            <Typography sx={{ fontWeight: 300, fontSize: '36px', color: '#fff', textShadow: '0px 0px 5px #D90280' }}>
                {'PARTY'}
                <span style={{ color: '#ED1F98', fontWeight: 500, marginLeft: 8 }}>
                    {'ZONE'}
                </span>
            </Typography>
            <Typography variant={'body2'} sx={{ color: '#fff', textAlign: 'center' }}>
                {'Get bombarded with our super vast game type collection'}
            </Typography>
        </Box>
        <Box >
            <SliderSection />
        </Box>
        <Box display={'flex'} alignItems={'center'} justifyContent={'center'} mt={5} mb={3}>
            <Button sx={{
                color: '#000',
                bgcolor: '#fff',
                borderRadius: 2,
                width: { lg: "20%", md: "30%", sm: "30%", xs: "50%" }
            }}>
                {'BOOK A PARTY'}
            </Button>
        </Box>
    </Box>

  );
};
