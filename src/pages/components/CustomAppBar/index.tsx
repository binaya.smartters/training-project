/* eslint-disable @next/next/no-img-element */
import {
  AppBar,
  Toolbar,
  Box,
  Hidden,
  IconButton,
  Button,
  useMediaQuery,
  Stack,
  Typography
} from "@mui/material";
import React from "react";
import MenuIcon from "@mui/icons-material/Menu";
import CustomSearch from "../CustomSearch";
import { useTheme } from '@mui/material/styles';

function CustomAppBar() {
  const theme = useTheme()
  const nerrowMobile = useMediaQuery('(max-width:300px)');
  const tablet = useMediaQuery(theme.breakpoints.only('sm'));
  const mobile = useMediaQuery(theme.breakpoints.only('xs'));
  const desktop = useMediaQuery(theme.breakpoints.up('md'));

  const navItems = [
    {
        name: "Menu"
    },
    {
        name: "Contact"
    },
    {
        name: "About"
    },

  ]

  return (
    <>
        <Stack justifyContent={'center'} alignItems={'center'} zIndex={40}>
                <Stack justifyContent={'center'} alignItems={'center'} sx={{ position: "fixed", top: 0, left: 0, width: '100%', zIndex: 10, height: { xs: 20, sm: 70, md: 70, lg: 100 } }}>
                    {
                        mobile &&
                        <img src={"/image/mobile-navbar-bg.svg"} height={'auto'} width={'100%'} />
                    }
                    {
                        tablet &&
                        <img src={"/image/tablet-navbar-bg.svg"} height={'auto'} width={'100%'} />
                    }
                    {
                        desktop &&
                        <img src={"/image/navbar-desktop.svg"} height={'auto'} width={'100%'} />
                    }
                </Stack>
                <Stack sx={{ height: { xs: 40, sm: 70, md: 70, lg: 110 }, width: "90%", pt: { xs: 1, sm: 1, md: 0, lg: 2 }, zIndex: 10, display: "flex", justifyContent: "space-between", position: 'fixed', top: 0 }}>
                    <Box sx={{ display: "flex", justifyContent: "space-between" }}>
                          <Box sx={{ display: "flex", alignItems: 'center', gap: 2 }}>
                              <Hidden smUp>
                                  <img src={"/icons/drawer.svg"} height={15} />
                              </Hidden>
                              <img src={"/image/logo.svg"} height={50} />
                              <Hidden>
                                  <CustomSearch />
                              </Hidden>
                          </Box>
                          <Box sx={{ display: "flex", alignItems: 'center', justifyContent: "flex-end" }}>
                              <Hidden smDown>
                                  {
                                      navItems.map((e, i) => (
                                          <Typography key ={i} color={'#FFFFFF'} sx={{ textTransform: "uppercase", mr: 2, cursor: 'pointer' }}>{e?.name}</Typography>
                                      ))
                                  }
                              </Hidden>

                              <Box display={'flex'} gap={2}>
                                  <Button sx={{ border: "1px solid #FFFFFF", borderRadius: "12px", color: '#FFFFFF' }}>OUR STORE</Button>
                                  <Hidden smDown>
                                      <Button variant="contained" sx={{ bgcolor: "#D90280", borderRadius: "12px" }}>BOOK TABLE</Button>
                                  </Hidden>
                              </Box>
                          </Box>
                      </Box>
                </Stack>
        </Stack>

    </>
  );
}

export default CustomAppBar;
