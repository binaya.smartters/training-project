import React from "react";
import Slider, { Settings } from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

interface Props {
  settings: Settings;
  children: React.ReactNode;
}

export const CustomSlider: React.FC<Props> = ({ settings, children }) => {
  return (
    <>
    <Slider {...settings}>
      {React.Children.map(children, (child) => (
        <div>{child}</div>
      ))}
    </Slider> 
    </>
  );
}

export default CustomSlider;
