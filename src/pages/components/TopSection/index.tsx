/* eslint-disable @next/next/no-img-element */
import { Box, Button, Hidden, InputAdornment, Typography } from "@mui/material";
import React from "react";

function TopSection() {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          position: "relative",
          mt: { lg: "5%",md: "3%", sm: "10%", xs: "20%" },
          height: { md: "350px", sm: "0px", xs: "180px" },
          width: "100%",
          zindex:1
        }}
      >
        <Hidden smDown>
          <img
            src={"/images/Frame 48.svg"}
            alt={"Image"}
            style={{ display: "hidden", width: "93%", height: "auto" }}
          />
        </Hidden>
        <Hidden smUp>
          <img
            src={"/images/Rectangle 1051.svg"}
            alt={"Image"}
            style={{ display: "hidden", width: "100%", height: "auto" }}
          />
        </Hidden>
        <Box
          sx={{
            position: "absolute",
            left: 0,
            top: 0,
            zIndex: 100,
            width: "100%",
            height: "auto",
            mt: { md: 10 },
          }}
        >
          <Box
            sx={{
              mb: 2,
              color: "#FFFFFF",
              fontWeight: 400,
              fontSize: "12px",
              ml: { md: 10 },
              display: "flex",
              alignItems: "center",
              justifyContent: { sm: "center", xs: "center", md: "flex-start" },
              mt: {lg:0, md:0,sm:0, xs:5}
            }}
          >
            <Hidden smDown>
              <img src={"/images/Stroke 1.svg"} alt={"Image"} />
            </Hidden>
            <Typography sx={{ ml: { md: 1 } }}>{"+91-9556266574,"}</Typography>
            <Typography sx={{ ml: 1 }}>{"+91-9556266575"}</Typography>
          </Box>
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexDirection: "column",
              mt: { md: 5, sm: 0, xs: 1 },
            }}
          >
            <Typography
              sx={{
                fontFamily: "Space Grotesk",
                fontWeight: 300,
                fontSize: "20px",
                textShadow: "0px 0px 5px #D90280",
                lineHeight: "116%",
                wordBreak: "break-word",
                p: 1,
                letterSpacing: "-0.02em",
                color: "#FFFFFF",
              }}
            >
              {"WELCOME TO"}
              <span
                style={{
                  fontFamily: "Space Grotesk",
                  fontStyle: "normal",
                  fontWeight: 700,
                  fontSize: "20px",
                  lineHeight: "116%",
                  letterSpacing: "-0.02em",
                  color: "#ED1F98",
                  textShadow: "0px 0px 5px #D90280",
                }}
              >
                {" QREW"}
              </span>
            </Typography>
            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                flexDirection: "column",
                textAlign: "center",
              }}
            >
              <Typography
                sx={{
                  background:
                    "linear-gradient(180deg, #FAFFF3 16.07%, #E8C473 32.59%, #D9B568 42.69%, #C49013 45.15%, #E6D76D 54.35%, #FFFFFF 65.84%, #DBB554 77.14%);",
                  fontFamily: "Montserrat",
                  fontWeight: 800,
                  width:"100%",
                  fontSize: { lg: "110px", md: "80px", sm: "51px", xs: "51px" },
                  textShadow: "0px 0px 23px rgba(228, 193, 114, 0.23)",
                  "-webkit-background-clip": "text",
                  "-webkit-text-fill-color": "transparent",
                  backgroundClip: "text",
                  textFillColor: "transparent",
                  lineHeight: "116%",
                  wordBreak: "break-word",
                }}
              >
                {"GROW YOUNG AGAIN"}
              </Typography>
              <Button
                sx={{
                  background:
                    "linear-gradient(180deg, #F31898 0%, #C80075 100%)",
                  color: "white",
                  borderRadius: "15px",
                  p: 1,
                  mt: 1,
                  width: { lg: "18%", md: "18%", sm: "90%", xs: "90%" },
                }}
              >
                BOOK YOUR TABLE*
              </Button>
              <Typography
                variant={"caption"}
                sx={{
                  color: "#798AED",
                  mt: 2,
                  ml: { md: 0, sm: 0, xs: 2 },
                  mr: { md: 0, sm: 0, xs: 2 },
                }}
              >
                {"*Table booking is only applicable for 21 years old or elder"}
              </Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
}

export default TopSection;
