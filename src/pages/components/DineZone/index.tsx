/* eslint-disable @next/next/no-img-element */
import { Box, Button, Grid, Hidden, Stack, Typography } from "@mui/material";
import React from "react";
import { DineZoneSlider } from "../DineZoneSlider";
function DineZone() {
  const card = [
    {
      attachment: "/images/Rectangle 1084.svg",
      title: "PC/ CONSOLE/ ESPORTS",
      subtitle: "VR RACING/DRIVING SIMULATOR",
    },
    {
      attachment: "/images/Rectangle 1084.svg",
      title: "PC/ CONSOLE/ ESPORTS",
      subtitle: "VR RACING/DRIVING SIMULATOR",
    },
    {
      attachment: "/images/Rectangle 1084.svg",
      title: "PC/ CONSOLE/ ESPORTS",
      subtitle: "VR RACING/DRIVING SIMULATOR",
    },
    {
      attachment: "/images/Rectangle 1084.svg",
      title: "PC/ CONSOLE/ ESPORTS",
      subtitle: "VR RACING/DRIVING SIMULATOR",
    },
    {
      attachment: "/images/Rectangle 1084.svg",
      title: "PC/ CONSOLE/ ESPORTS",
      subtitle: "VR RACING/DRIVING SIMULATOR",
    },
  ];

  return (
    <DineZoneSlider
      background={"/images/Group 1471 (1).svg"}
      direction={"rtl"}
      containerYPadding={"24px"}
      right={"8px"}
      left={"0"}
      cardPadding={"16px"}
      topPackagesList={card}
    >
      <Grid item xs={12} sm={5}>
        <Stack
          direction={"row"}
          sx={{
            width: { lg: "60%", md: "60%", sm: "60%", xs: "100%" },
            // pl: 4
          }}
        >
          <Box sx={{ width: { lg: "100%", md: "60%", sm: "100%", xs: "50%" } }}>
            <img
              src={"/image/dine-mobile.svg"}
              alt={"Image"}
              style={{ width: "100%", height: "auto" }}
            />
          </Box>
          <Hidden smUp>
            <Button
              variant="contained"
              size="small"
              sx={{
                bgcolor: "#D90280",
                borderRadius: "12px",
                width: { xs: "60%", sm: "75%", md: "75%", lg: "75%" },
                mt: { xs: 1, sm: 2, md: 3, lg: 5 },
                height: "30px",
              }}
            >
              VIEW MENU
            </Button>
          </Hidden>
        </Stack>
        <Typography variant="subtitle1" sx={{ mt: 3, color: "#ffffff" }}>
          Lorem ipsum dolor sit amet consectetur. At ultrices neque nisl vitae
          tristique tincidunt sit arcu. Proin nunc sed sem nibh mattis urna
          fames.
        </Typography>
        <Hidden smDown>
          <Button
            variant="contained"
            sx={{
              bgcolor: "#D90280",
              borderRadius: "12px",
              width: { xs: "50%", sm: "75%", md: "75%", lg: "75%" },
              mt: { xs: 0, sm: 2, md: 3, lg: 5 },
            }}
          >
            BROWSE OUR MENU
          </Button>
        </Hidden>
      </Grid>
    </DineZoneSlider>
  );
}

export default DineZone;
