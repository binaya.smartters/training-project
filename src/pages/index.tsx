import type { NextPage } from 'next'
import Qrew from './qrew'
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const Home: NextPage = () => {
  return (
    <>
      <Qrew />
    </>
  )
}

export default Home
